'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var cssmin = require('gulp-cssmin');
var autoprefixer = require('gulp-autoprefixer');
var livereload = require('gulp-livereload');
var wiredep = require('wiredep').stream;
var connect = require('gulp-connect');

gulp.task('sass', function () {
  gulp.src('scss/*.scss')
    .pipe(sass())
    .on('error', swallowError)
    .pipe(autoprefixer({
            browsers: ['last 20 versions'],
            cascade: false
        }))
    .pipe(gulp.dest('./css'))
    .pipe(connect.reload());
});

gulp.task('minify-css', function() {
  return gulp.src('css/*.css')
    .pipe(cssmin())
    .on('error', swallowError)
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('css/min'))
    .pipe(livereload());
});

gulp.task('connect', function() {
  connect.server({
    livereload: true,
    port: 8888
  });
});

// gulp.task('bower', function () {
//   gulp.src('*.html')
//     .pipe(wiredep({
//       directory: "lib"
//     })).on('error', swallowError)
//     .pipe(gulp.dest('./'));
// });

gulp.task('watch', function () {
  // livereload.listen();
  gulp.watch('scss/*.scss', ['sass']);
  gulp.watch('css/*.css', ['minify-css']);
  gulp.watch('bower.json', ['bower']);
});

gulp.task('server', ['connect', 'watch' ]);

function swallowError (error) {
  console.log(error.toString());
  this.emit('end');
}
$(document).ready(function($) {
	if (Modernizr.touchevents) {
		$('.aboutMeRightCol').waypoint({
			handler: function (direction) {
					$('.aboutMeRightCol').addClass('isActive');
					$('#contactIco2').drawsvg({
						duration: 2500,
						stagger: 300,
					}).drawsvg('animate');

					$('#contactIco').drawsvg({
						duration: 1200,
						stagger: 300,
					}).drawsvg('animate');

					this.destroy()
				},
			offset: '50%'
		});
	}else {
		$('.aboutMeRightCol').waypoint({
			handler: function (direction) {
				$('.aboutMeRightCol').addClass('isActive');
					$('#contactIco2').drawsvg({
						duration: 2500,
						stagger: 300,
					}).drawsvg('animate');

					$('#contactIco').drawsvg({
						duration: 1200,
						stagger: 300,
					}).drawsvg('animate');

					this.destroy()
				},
			context: '.wrapper',
			offset: '50%'
		});
	}
});
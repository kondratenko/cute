$(document).ready(function($) {
	$('.projectsUl li:first-child').click(function(event) {
		$(this).closest('.projectsUl').toggleClass('isOpen');
	});

	if (Modernizr.touchevents) {
		$('.mapWrapper').waypoint({
			handler: function (direction) {
				$('.mapWrapper').addClass('isActive');
				$('#island').drawsvg({
					duration: 900,
					stagger: 300,
				}).drawsvg('animate');

				$('#mainland').drawsvg({
					duration: 3300,
					stagger: 300,
				}).drawsvg('animate');

				$('#europe').drawsvg({
					duration: 800,
					stagger: 100,
				}).drawsvg('animate');

				$('#otherEurope').drawsvg({
					duration: 2000,
					stagger: 200,
				}).drawsvg('animate');

				this.destroy()
				},
			offset: '80%'
		});
	}else {
		$('.mapWrapper').waypoint({
			handler: function (direction) {
				$('.mapWrapper').addClass('isActive');
				$('#island').drawsvg({
					duration: 900,
					stagger: 300,
				}).drawsvg('animate');

				$('#mainland').drawsvg({
					duration: 3300,
					stagger: 300,
				}).drawsvg('animate');

				$('#europe').drawsvg({
					duration: 800,
					stagger: 100,
				}).drawsvg('animate');

				$('#otherEurope').drawsvg({
					duration: 2000,
					stagger: 200,
				}).drawsvg('animate');

				this.destroy()
				},
			context: '.wrapper',
			offset: '80%'
		});
	}

	$('path').on("mouseover", function() {
		this.parentNode.appendChild(this);
	});

	$('.projectPlus').on('click', function(event) {
		$(this).closest('li').toggleClass('isActive');
	});

	$('.projectsUl').each(function() {
		var thisLi = $(this).find('li').slice(1);
		// thisLi.slice(0, Math.ceil(thisLi.length/2)).wrapAll('<ul class="leftLi"></ul>');
		// thisLi.slice(Math.ceil(thisLi.length/2), thisLi.length).wrapAll('<ul class="rightLi"></ul>');
	});



});
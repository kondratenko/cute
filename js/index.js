$(document).ready(function($) {
	if (!Modernizr.svg) {
		var imgs = document.getElementsByTagName('img');
		var svgExtension = /.*\.svg$/
		var l = imgs.length;
		for(var i = 0; i < l; i++) {
			if(imgs[i].src.match(svgExtension)) {
				imgs[i].src = imgs[i].src.slice(0, -3) + 'png';
			}
		}
	}

	if (Modernizr.touchevents) {
		$('.indicate').waypoint({
			handler: function (direction) {
				$('.plus').toggleClass('isActive');
			},
			offset: '0%'
		});
	}else {
		$('.indicate').waypoint({
			handler: function (direction) {
				$('.plus').toggleClass('isActive');
			},
			context: '.wrapper',
			offset: '0%'
		});
	}

	setTimeout(function(){
		$('.plus').removeClass('isActive');
	}, 30);
});
$(document).ready(function($) {
	var body = $('body');
	var html = $('html');
	var wrapper = $('.wrapper');
	var mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
	var notLinks = $('.menuWrapper a');
	var videoWrapp = $('.videoWrapp');

	setTimeout(function(){
		body.addClass('isLoaded').css({'opacity':'1'});
		$('.plus').addClass('isActive');
	}, 30);

	notLinks.click(function(event) {
		var href = $(this).attr('href')
		event.preventDefault();
		body.addClass('blur');
		setTimeout(function() { 
			window.location = href;
		}, 500)
	});
	if (!Modernizr.cssvwunit) {
		function calcVw() {
			var calcX = body.width();
			var vw;
			switch ( true ) {
				case calcX >= 1024:
					vw = calcX/100;
					break
				case calcX >= 641 && calcX < 1024:
					vw = calcX/100*1.05;
					break
				case calcX < 640:
					vw = calcX/100*1.06;
					break
				default:
					vw = calcX/100;
					break
			}

			html.css({
				'font-size' : vw + 'px'
			});
		}
		calcVw();

		$(window).resize(function(){
			calcVw();
		});
	}

	if ( videoWrapp.length == 1) {
		var video = videoWrapp.vide({
				mp4: '/video/video',
				poster: '/video/video',
			}, {
				volume: 1,
				playbackRate: 1,
				muted: true,
				loop: true,
				autoplay: true,
				position: '50% 50%',
				resizing: true,
				bgColor: 'transparent'
		});
	};

	$('.plus').on('click', function(event) {
		if ( !$(this).hasClass('isOpen') ) {
			$(this).add('.menuWrapper').addClass('isOpen');
			disableScroll();
		}
		else {
			$(this).add('.menuWrapper').removeClass('isOpen');
			enableScroll();
		}
	});

	if (Modernizr.touchevents){

		body.on("touchstart touchmove touchend", function(e) {
			if ( e.originalEvent.touches.length > 1 ) {
				e.preventDefault();
			}
		});

		$('[data-src]').waypoint({
			handler: function () {
				if ( this.element.localName == 'img') {
					$(this.element).attr({ src: this.element.dataset.src });
				}else if ( this.element.localName == 'div' || this.element.localName == 'a' ) {
					$(this.element).css({ 'backgroundImage': 'url('+this.element.dataset.src+')' });
				}

				this.destroy()
				},
			offset: '200%'
		});
	}else {
		$('[data-src]').waypoint({
			handler: function () {
				if ( this.element.localName == 'img') {
					$(this.element).attr({ src: this.element.dataset.src });
				}else if ( this.element.localName == 'div' || this.element.localName == 'a' ) {
					$(this.element).css({ 'backgroundImage': 'url('+this.element.dataset.src+')' });
				}

				this.destroy()
				},
			context: '.wrapper',
			offset: '200%'
		});
	}

	if ( $('#fb-root').length >= 1 ) {
		window.fbAsyncInit = function() {
			FB.init({appId: '469975366524354', status: true, cookie: true,
			xfbml: true});
			};
			(function() {
			var e = document.createElement('script'); e.async = true;
			e.src = document.location.protocol +
			'//connect.facebook.net/en_US/all.js';
			document.getElementById('fb-root').appendChild(e);
		}());
	}

	if (!Modernizr.touchevents) {
		$(".goTop").click(function (e) {
			$('.wrapper').animate({
				scrollTop: 0
			}, 700, "linear");
			e.preventDefault();
			e.stopPropagation();
			wrapper.slimScroll({ scrollTo: '0px' });
		});
	}else {
		$(".goTop").click(function (e) {
			$('html, body').animate({
				scrollTop: 0
			}, 700, "linear");
			e.preventDefault();
			e.stopPropagation();
		});
	}

	var ms_ie = false;
	var ua = window.navigator.userAgent;
	var old_ie = ua.indexOf('MSIE ');
	var new_ie = ua.indexOf('Trident/');

	if ((old_ie > -1) || (new_ie > -1)) {
		ms_ie = true;
	}


	if (!Modernizr.svg || ms_ie ) {
		var imgs = $('nav img');
		var svgExtension = /.*\.svg$/
		var l = imgs.length;
		for(var i = 0; i < l; i++) {
			if(imgs[i].src.match(svgExtension)) {
				imgs[i].src = imgs[i].src.slice(0, -3) + 'png';
				console.log(imgs[i].src);
			}
		}
	}

	var keys = {37: 1, 38: 1, 39: 1, 40: 1};

	function preventDefault(e) {
		e = e || window.event;
		if (e.preventDefault)
			e.preventDefault();
		e.returnValue = false;  
	}

function preventDefaultForScrollKeys(e) {
		if (keys[e.keyCode]) {
			preventDefault(e);
			return false;
		}
	}

	function disableScroll() {
		if (window.addEventListener)
			window.addEventListener('DOMMouseScroll', preventDefault, false);
		window.onwheel = preventDefault;
		window.onmousewheel = document.onmousewheel = preventDefault;
		window.ontouchmove  = preventDefault;
		document.onkeydown  = preventDefaultForScrollKeys;
	}

	function enableScroll() {
		if (window.removeEventListener)
			window.removeEventListener('DOMMouseScroll', preventDefault, false);
		window.onmousewheel = document.onmousewheel = null;
		window.onwheel = null;
		window.ontouchmove = null;
		document.onkeydown = null;
	}
});
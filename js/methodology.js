$(document).ready(function($) {
	var notLinks = $('.menuWrapper a');

	var arrayColorsMethodology = [ '#B09766' ]

	var arrayColors = [ '#B09769',
						'#D9DD00',
						'#FFD1D8',
						'#3678E9',
						'#3EE2A4',
						'#ED145B',
						'#11D3C8',
						'#FFBF43',
						'#EF008C',
						'#10BBF6',
						'#0837D0',
						'#6734BA'
						]

	$('.followMe a').not(notLinks).hover(function(){
		thisColor = $(this).css('color');
		var rand = Math.floor(Math.random() * arrayColors.length);
		$(this).css({'color': arrayColors[rand] });
	}, function() {
		$(this).css({'color': thisColor});
	});

	$('a').not('.followMe a').not(notLinks).hover(function(){
		thisColor = $(this).css('color');
		var rand = Math.floor(Math.random() * arrayColorsMethodology.length);
		$(this).css({'color': arrayColorsMethodology[rand] });
	}, function() {
		$(this).css({'color': thisColor});
	});

	$('.openMethTab').on('click', function(event) {
		event.preventDefault();
		$(this).closest('.methState').toggleClass('isActive');
	});

	$('.plus').css({'borderColor': arrayColorsMethodology[0] })
	var params = {
		size: '3px',
		opacity : 1,
		disableFadeOut : true,
		railVisible : true,
		color: arrayColorsMethodology[0],
		railColor : '#000000',
		railOpacity: 1,
		distance: 0,
		wheelStep: 10,
		borderRadius: '0px',
		railBorderRadius : '0px'
	}

	if (!Modernizr.touchevents) {
		var wrapper = $('.wrapper');
		wrapper.slimScroll( params );

		setTimeout(function(){
			wrapper.mouseenter();
		}, 30);

		var scrollBar = $('.slimScrollRail, .slimScrollBar');
		var scrollHover = $('.scrollHover');

		$('.scrollHover').add(scrollBar).mouseover(function(event) {
			scrollBar.addClass('isActive');
		});

		$('.scrollHover').mouseleave(function(event) {
			scrollBar.removeClass('isActive');
		});

		$('.slimScrollBar').click(function(event) {
			event.stopPropagation();
		});

		$('.slimScrollRail').click(function(event) {
			var railH = $(this).height();
			var bar = $('.slimScrollBar');
			var topCord = bar.offset().top;
			var bottomCord = bar.offset().top + bar.height();
			var relativeY = event.pageY - $(this).offset().top;

			if( relativeY > bottomCord) {
				wrapper.slimScroll({
					scrollBy: railH*0.8+'px'
				});
				scrollBar.addClass('isActive');
			}else if ( topCord > relativeY ) {
				wrapper.slimScroll({
					scrollBy: -railH*0.8+'px'
				});
				scrollBar.addClass('isActive');
			}
			setTimeout(function(){
				$(this).mouseenter();
			}, 30);
		});

	}else {
		$('body').css({'overflow':'visible'})
	}


	if (!Modernizr.touchevents) {
		$(".goTop").click(function (e) {
			$('.wrapper').animate({
				scrollTop: 0
			}, 700, "linear");
			e.preventDefault();
			e.stopPropagation();
			wrapper.slimScroll({ scrollTo: '0px' });
		});
	}else {
		$(".goTop").click(function (e) {
			$('html, body').animate({
				scrollTop: 0
			}, 700, "linear");
			e.preventDefault();
			e.stopPropagation();
		});
	}

	if ( $(".theVideo").length >= 1 ) {
		$(".theVideo").jqueryVideoLightning({
			autoplay: 1,
			backdrop_color: "#ddd",
			backdrop_opacity: 0.8,
			bdColor: '#000000',
			glow_color: "none",
			width: '100%',
			height: '56.25rem',
			fadeIn: 600,
			fadeOut: 300,
			showinfo : 0,
			xBgColor: 'transparent',
			xColor: 'transparent'
		});
	}

});
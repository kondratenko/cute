/*
 * jquery-instagram
 * http://potomak.github.com/jquery-instagram
 *
 * Copyright (c) 2013 Giovanni Cappellotto
 * Licensed under the MIT license.
 */

(function($) {

  function composeRequest(options) {
    var url = 'https://api.instagram.com/v1';
    var data = {};

    if (options.accessToken == null && options.clientId == null) {
      throw 'You must provide an access token or a client id';
    }

    data = $.extend(data, {
      access_token: options.accessToken || '',
      client_id: options.clientId || '',
      count: options.count || ''
    });

    if (options.url != null) {
      url = options.url;
    }
    else if (options.hash != null) {
      url += '/tags/' + options.hash + '/media/recent';
    }
    else if (options.search != null) {
      url += '/media/search';
      data = $.extend(data, options.search);
    }
    else if (options.userId != null) {
      if (options.accessToken == null) {
        throw 'You must provide an access token';
      }
      url += '/users/' + options.userId + '/media/recent';
    }
    else if (options.location != null) {
      url += '/locations/' + options.location.id + '/media/recent';
      delete options.location.id;
      data = $.extend(data, options.location);
    }
    else {
      url += '/media/popular';
    }
    
    return {url: url, data: data};
  }

  $.fn.instagram = function(options) {
    var that = this;
    options = $.extend({}, $.fn.instagram.defaults, options);
    var request = composeRequest(options);

    $.ajax({
      dataType: "jsonp",
      url: request.url,
      data: request.data,
      success: function(response) {
        that.trigger('didLoadInstagram', response);
      }
    });

    this.trigger('willLoadInstagram', options);
    
    return this;
  };

  $.fn.instagram.defaults = {
    accessToken: '2333292276.1677ed0.bb51a30cc4484f9199ac864e3ce1ddf4',
    clientId: 'feee5e0329034ef49ae56f039bdbff6d',
    count: null,
    url: null,
    hash: null,
    userId: null,
    location: null,
    search: null
  };

}(jQuery));

$(document).ready(function($) {
	$('body').css({'opacity':1})
	var servicesUl = $('.servicesUl');

	servicesUl.owlCarousel({ 
		center: false,
		loop:false,
		margin: 0,
		dragEndSpeed: false,
		smartSpeed: 400,
		responsive : {
			0 : {
				items:1,
			},
			641 : {
				items:3,
			},
			1025 : {
				items:5,
			}
		}
	});

});
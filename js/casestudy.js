$(document).ready(function($) {
	var theWindow = $(window);
	var sliderWrap = $('.mobileSliderWrapper');
	var servicesUl = $('.servicesUl');
	var smallImgWrapper = $('.smallImgWrapper');

	if ( sliderWrap.length >= 1 ) {
		sliderWrap.owlCarousel({ 
			center: true,
			loop:false,
			margin: 40,
			responsiveClass: false,
			autoHeight:false,
			autoWidth: true,
			responsive: {
				0 : {
					items:1,
				},
				641 : {
					items:2,
				},
				1200 : {
					items:3,
				}
			}
		});
	}

	if ( smallImgWrapper.length >= 1 ) {
		smallImgWrapper.owlCarousel({ 
			center: false,
			loop:false,
			margin: 30,
			responsiveClass: false,
			autoHeight:false,
			autoWidth: false,
			responsive: {
				0 : {
					items:3,
				},
				1025 : {
					items:1,
				}
			}
		});
	}

	servicesUl.owlCarousel({ 
		center: false,
		loop:false,
		margin: 0,
		responsive : {
			0 : {
				items:1,
			},
			641 : {
				items:3,
			},
			1025 : {
				items:5,
			}
		}
	});

	if ( $('.imagesProjectWrapper .thegrid').length >= 1 ) {
		if (Modernizr.touchevents) {
			$('.imagesProjectWrapper').waypoint({
				handler: function (direction) {
					$('.imagesProjectWrapper').addClass('isActive');
					$('.thegrid').drawsvg({
						duration: 900,
						stagger: 250,
					}).drawsvg('animate');


					this.destroy()
					},
				offset: '80%'
			});
		}else {
			$('.imagesProjectWrapper').waypoint({
				handler: function (direction) {
					$('.imagesProjectWrapper').addClass('isActive');
					$('.thegrid').drawsvg({
						duration: 900,
						stagger: 250,
					}).drawsvg('animate');
					},
				context: '.wrapper',
				offset: '80%'
			});
		}
	}

});
$(document).ready(function($) {
	var nextUrl;

	jQuery(function($) {
		function didLoadInstagram(event, response) {
			var that = this;
			// console.log(response);
			nextUrl = response.pagination.next_url;
			$.each(response.data, function(i, photo) {
				$(that).append(createPhotoElement(photo));
			});
		}

		function createPhotoElement(photo) {
			var wrapper = $('.backstageWrapper');
			var box = $('<div>').addClass('backstage').attr('id', photo.id);
			var link = $('<a>').attr('target', '_blank').attr('href', photo.link).css({'background-image': 'url('+photo.images.standard_resolution.url+')'}).appendTo(box);
			return box.appendTo(wrapper);
		}

		$('.backstageWrapper').on('didLoadInstagram', didLoadInstagram);

		$('.backstageWrapper').instagram({
			// hash: 'barselona',
			count: 31,
			clientId: 'feee5e0329034ef49ae56f039bdbff6d',
			userId: 460659733
			// userId: 2333292276
		});
	});

	$('.moreBackStage').click(function(event) {
		$('.backstageWrapper').instagram({
			count: 20,
			clientId: 'feee5e0329034ef49ae56f039bdbff6d',
			userId: 460659733,
			url: nextUrl
		});
	});

});
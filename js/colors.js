$(document).ready(function($) {
	var wrapper = $('.wrapper');
	var notLinks = $('.menuWrapper a').add('.goldContactLink');
	var arrayColors = [ '#B09769',
						'#D9DD00',
						'#FFD1D8',
						'#3678E9',
						'#3EE2A4',
						'#ED145B',
						'#11D3C8',
						'#FFBF43',
						'#EF008C',
						'#10BBF6',
						'#0837D0',
						'#6734BA'
						];

	$('a').not(notLinks).hover(function(){
		if ( $(this).attr('href') === undefined ) {
			return false;
		}
		thisColor = $(this).css('color');
		var rand = Math.floor(Math.random() * arrayColors.length);
		$(this).css({'color': arrayColors[rand] });
	}, function() {
		$(this).css({'color': thisColor});
	});

	var randRailColor = Math.floor(Math.random() * arrayColors.length);
	$('.plus, .goTop').css({'borderColor': arrayColors[randRailColor] });
	if ( $('.sharePageWrapper').length >= 1 ) {
		$('.sharePageWrapper').css({'color':arrayColors[randRailColor]});
	}

	var params = {
		size: '3px',
		opacity : 1,
		disableFadeOut : true,
		railVisible : true,
		color: arrayColors[randRailColor],
		railColor : '#000000',
		railOpacity: 1,
		distance: 0,
		wheelStep: 10,
		borderRadius: '0px',
		railBorderRadius : '0px'
	}
	if (!Modernizr.touchevents) {
		wrapper.slimScroll( params );

		setTimeout(function(){
			wrapper.mouseenter();
		}, 30);

		var scrollBar = $('.slimScrollRail, .slimScrollBar');
		var scrollHover = $('.scrollHover');

		$('.scrollHover').add(scrollBar).mouseover(function(event) {
			scrollBar.addClass('isActive');
		});

		$('.scrollHover').mouseleave(function(event) {
			scrollBar.removeClass('isActive');
		});

		$('.slimScrollBar').click(function(event) {
			event.stopPropagation();
		});

		$('.slimScrollRail').click(function(event) {
			var railH = $(this).height();
			var bar = $('.slimScrollBar');
			var topCord = bar.offset().top;
			var bottomCord = bar.offset().top + bar.height();
			var relativeY = event.pageY - $(this).offset().top;

			if( relativeY > bottomCord) {
				wrapper.slimScroll({
					scrollBy: railH*0.8+'px'
				});
				scrollBar.addClass('isActive');
			}else if ( topCord > relativeY ) { 
				wrapper.slimScroll({
					scrollBy: -railH*0.8+'px'
				});
				scrollBar.addClass('isActive');
			}
			setTimeout(function(){
				$(this).mouseenter();
			}, 30);
		});

	}else {
		$('body').css({'overflow':'visible'})
	}

	if ( $(".theVideo").length >= 1 ) {
		$(".theVideo").jqueryVideoLightning({
			autoplay: 1,
			backdrop_color: "#ddd",
			backdrop_opacity: 0.8,
			bdColor: '#000000',
			glow_color: "none",
			width: '100%',
			height: '56.25rem',
			fadeIn: 600,
			fadeOut: 300,
			showinfo : 0,
			xBgColor: 'transparent',
			xColor: 'transparent'
		});
	}

	if(window.location.hash) {
		var idAnchor = (''+window.location.hash).substr(1);
		var heightAnim = $('[id=' + idAnchor + ']').offset().top;
		if (!Modernizr.touchevents) {
				$('.wrapper').animate({
					scrollTop: heightAnim
				}, 1200, "swing");
				wrapper.slimScroll({ scrollTo: heightAnim });
		}else {
			$('html, body').animate({
				scrollTop: heightAnim
			}, 1200, "swing");
		}
	}

});
$(document).ready(function($) {

	var wrapper = $('.wrapper');
	var notLinks = $('.menuWrapper a');
	var arrayColors = [ '#B09769',
						'#D9DD00',
						'#FFD1D8',
						'#3678E9',
						'#3EE2A4',
						'#ED145B',
						'#11D3C8',
						'#FFBF43',
						'#EF008C',
						'#10BBF6',
						'#0837D0',
						'#6734BA'
						];

	$('a').not(notLinks).hover(function(){
		thisColor = $(this).css('color');
		var rand = Math.floor(Math.random() * arrayColors.length);
		$(this).css({'color': arrayColors[rand] });
	}, function() {
		$(this).css({'color': thisColor});
	});

	var randRailColor = Math.floor(Math.random() * arrayColors.length);
	if ( $('.sharePageWrapper').length >= 1 ) {
		$('.sharePageWrapper').css({'color': '#31313F'});
	}

	var videoYellow = $('.wrapper').vide({
			mp4: '/video/awards',
			poster: '/video/awards',
		}, {
			volume: 1,
			playbackRate: 1,
			muted: true,
			loop: true,
			autoplay: true,
			position: '50% 50%',
			resizing: true,
			bgColor: 'transparent'
	});

	if ( $(".theVideo").length >= 1 ) {
		$(".theVideo").jqueryVideoLightning({
			autoplay: 1,
			backdrop_color: "#ddd",
			backdrop_opacity: 0.8,
			bdColor: '#000000',
			glow_color: "none",
			width: '100%',
			height: '56.25rem',
			fadeIn: 600,
			fadeOut: 300,
			showinfo : 0,
			xBgColor: 'transparent',
			xColor: 'transparent'
		});
	}

});